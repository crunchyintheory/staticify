import { StaticifyHandler } from "./types";

export class StaticResourceHandler<T> {

    public constructor(public readonly method: string,
                        public readonly pattern: RegExp, 
                        public readonly apiHandler: StaticifyHandler<T>,
                        public readonly staticHandler: StaticifyHandler<T>) {
    }
}