import { StaticResourceHandler } from "./internal/StaticResourceHandler";
import { StaticifyHandler } from "./internal/types";

export abstract class Staticify {

    public static local = false;

    protected static handlers: Array<StaticResourceHandler<any>> = new Array;

    public static addHandler<T>(method: string, pattern: RegExp, apiHandler: StaticifyHandler<T>, staticHandler: StaticifyHandler<T>): void {
        this.handlers.push(new StaticResourceHandler<T>(method, pattern, apiHandler, staticHandler));
    }
    public static add<T>(method: string, pattern: RegExp, apiHandler: StaticifyHandler<T>, staticHandler: StaticifyHandler<T>): void {
        this.addHandler(method, pattern, apiHandler, staticHandler);
    }

    public static get<T>(pattern: RegExp, apiHandler: StaticifyHandler<T>, staticHandler: StaticifyHandler<T>): void {
        this.addHandler("get", pattern, apiHandler, staticHandler);
    }

    public static post<T>(pattern: RegExp, apiHandler: StaticifyHandler<T>, staticHandler: StaticifyHandler<T>): void {
        this.addHandler("post", pattern, apiHandler, staticHandler);
    }

    public static put<T>(pattern: RegExp, apiHandler: StaticifyHandler<T>, staticHandler: StaticifyHandler<T>): void {
        this.addHandler("put", pattern, apiHandler, staticHandler);
    }

    public static delete<T>(pattern: RegExp, apiHandler: StaticifyHandler<T>, staticHandler: StaticifyHandler<T>): void {
        this.addHandler("delete", pattern, apiHandler, staticHandler);
    }

    protected static getHandler<T>(method: string, uri: string): StaticResourceHandler<T> | undefined {
        return this.handlers.find(x => x.method === method && uri.match(x.pattern)) as StaticResourceHandler<T> | undefined;
    }

    public static invoke<T>(method: string, uri: string, args: any[]): T {
        let handler = this.getHandler<T>(method, uri);
        if(handler === undefined) {
            throw `No route handler defined for ${uri}`;
        }
        if(this.local)
            return handler.staticHandler(uri, ...args);
        else
            return handler.apiHandler(uri, ...args);
    }

}

export function staticify<T>(method: string, uri: string, args: any[]): T {
    return Staticify.invoke(method, uri, args);
}