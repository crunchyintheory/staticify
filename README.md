# staticify.js

Dynamically reroute network requests to a client-side handler under certain conditions.

## Installation

`npm i --save staticify.js`